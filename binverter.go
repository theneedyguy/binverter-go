package binverter

import (
	"os"
	"bufio"
	"log"
	"github.com/cheggaaa/pb/v3"
)

type BinaryFile struct {
	InputPath   string
	OutputPath  string
	FileSize    int64
	FileBuffer  []byte
}

// This method opens the file on the path defined in InputPath
// Then it returns a pointer to the file to be used in other methods
func (b *BinaryFile) OpenFile() *os.File {
	file, err := os.Open(b.InputPath)
	if err != nil {
		log.Fatalln("Could not open the file! Please provide a valid path or check if the file exists!")
		panic(err)
	}
	return file
}

// This method opens the file defined in the InputPath attribute of the struct via OpenFile()
// It then reads the file size to the FileSize attribute of the BinaryFile struct
func (b *BinaryFile) GetSizeOfFile() {
	// Open the file and read the file size to the BinaryFile struct
	file := b.OpenFile()
	defer file.Close()
	stats, err := file.Stat()
	if err != nil {
		log.Fatalln("Could not get file size!")
		b.FileSize = 0
	}
	b.FileSize = stats.Size()
}

// This method generates an empty slice the size of the original file
// The method GetSizeOfFile() gets to read the file size into the struct
func (b *BinaryFile) GenerateEmptySlice() {
	b.GetSizeOfFile()
	size := b.FileSize
	if size <= 0 {
		log.Fatalln("File size is less or equal to 0!")
		b.FileBuffer = nil
	}
	bytes := make([]byte, size)
	b.FileBuffer = bytes
}

// This method writes the file to a buffer. It first generates an emtpy slice the size of the file
// Then it populates the slice using the data of the original file
func (b *BinaryFile) WriteFileToBuffer() {
	// First there needs to be an empty slice with the size of the original file
	b.GenerateEmptySlice()
	// Open the file
	file  := b.OpenFile()
	defer file.Close()
	// Read the size of the file to an empty slice
	slice := b.FileBuffer
	// If the file could not be read panic
	if slice == nil {
		log.Fatalln("No empty slice could be created to be used as a buffer!")
		panic(slice)
	}
	// Create a buffer reader and read the data from the file to the empty slice
	buffer := bufio.NewReader(file)
	_, err := buffer.Read(slice)
	// If there is an error return nil
	if err != nil {
		log.Fatalln("Could not read file to buffer!")
	}
	b.FileBuffer = slice
}

// This function inverts the original file and writes the output to the OutputPath
// It does this by writing the file to a buffer then then uses the data inside the buffer
// The data gets inverted byte by byte and directly written to the output file
func (b *BinaryFile) InvertFile() {
	b.WriteFileToBuffer()
	size		:= b.FileSize
	originalFile	:= b.FileBuffer
	if b.OutputPath == "" {
		log.Fatalln("No output path was provided!")
		os.Exit(1)
	}
	// Remove output file first if it already exists. This prevents corrupted files
	var _, err = os.Stat(b.OutputPath)
	if err == nil {
		var err = os.Remove(b.OutputPath)
		if err != nil {
			log.Fatal(err)
			os.Exit(1)
		}
	}
	output, err  := os.OpenFile(b.OutputPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer output.Close()
	// New progressbar with count of filesize
	bar := pb.New(int(size))
	bar.Set(pb.Bytes, true)
	bar.SetWriter(os.Stdout)
	bar.Start()
	for i := 0; i < int(size); i++ {
		inverse := 1 - originalFile[i]
		bytearr := make([]byte, 1)

		bytearr[0] = inverse
		if _, err := output.Write(bytearr); err != nil {
			log.Fatal(err)
		}
		bar.Increment()
	}
	bar.Finish()
}

